package Commands;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.concurrent.TimeUnit;

public class ss {
    public static int callMe(MessageReceivedEvent event, String message, int counter) {
        if (message.equalsIgnoreCase("p!ss")) {
            event.getTextChannel().sendMessage(Constants.ssHelp).queue((message2 -> message2.delete().queueAfter(120, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Collision")) {
            event.getTextChannel().sendMessage(Constants.ssCollision).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Sock Attack")) {
            event.getTextChannel().sendMessage(Constants.ssSockAttack).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Chain Collision")) {
            event.getTextChannel().sendMessage(Constants.ssChainCollision).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Violent Asylum")) {
            event.getTextChannel().sendMessage(Constants.ssViolentAsylum).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Bleeding Wounds")) {
            event.getTextChannel().sendMessage(Constants.ssBleedingWounds).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Protection Impact")) {
            event.getTextChannel().sendMessage(Constants.ssProtectionImpact).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Healing")) {
            event.getTextChannel().sendMessage(Constants.ssHealing).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Guardian")) {
            event.getTextChannel().sendMessage(Constants.ssGuardian).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Substitute Defense")) {
            event.getTextChannel().sendMessage(Constants.ssSubstituteDefense).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss DMG Share")) {
            event.getTextChannel().sendMessage(Constants.ssDMGShare).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Holy Sanctuary")) {
            event.getTextChannel().sendMessage(Constants.ssHolySanctuary).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Passive Virus")) {
            event.getTextChannel().sendMessage(Constants.ssPassiveVirus).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Vloek")) {
            event.getTextChannel().sendMessage(Constants.ssVloek).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Target Guidance")) {
            event.getTextChannel().sendMessage(Constants.ssTargetGuidance).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ss Split Attack")) {
            event.getTextChannel().sendMessage(Constants.ssSplitAttack).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        }

        return counter;
    }
}
