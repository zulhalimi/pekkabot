package Commands;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.concurrent.TimeUnit;

public class pSkill {
    public static int callMe(MessageReceivedEvent event, String message,int counter) {
        if (message.equalsIgnoreCase("p!ps")) {
            event.getTextChannel().sendMessage(Constants.petSkillHelp).queue((message2 -> message2.delete().queueAfter(120, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps AS") || message.equalsIgnoreCase("p!ps Attack Strengthen")) {
            event.getTextChannel().sendMessage((Constants.pskillAS)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps AW") || message.equalsIgnoreCase("p!ps Awaken")) {
            event.getTextChannel().sendMessage((Constants.pskillAW)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps BS") || message.equalsIgnoreCase("p!ps Bloodshot")) {
            event.getTextChannel().sendMessage((Constants.pskillBS)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps CC") || message.equalsIgnoreCase("p!ps Concentration")) {
            event.getTextChannel().sendMessage((Constants.pskillCC)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps CE") || message.equalsIgnoreCase("p!ps Controlled Enhance")) {
            event.getTextChannel().sendMessage((Constants.pskillCE)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps CA") || message.equalsIgnoreCase("p!ps Counter Attack")) {
            event.getTextChannel().sendMessage((Constants.pskillCA)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps CU") || message.equalsIgnoreCase("p!ps Cure")) {
            event.getTextChannel().sendMessage((Constants.pskillCU)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps DS") || message.equalsIgnoreCase("p!ps Damage Share")) {
            event.getTextChannel().sendMessage((Constants.pskillDS)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps ED") || message.equalsIgnoreCase("p!ps Element Delay")) {
            event.getTextChannel().sendMessage((Constants.pskillED)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps AT") || message.equalsIgnoreCase("p!ps Attack Terminator")) {
            event.getTextChannel().sendMessage((Constants.pskillAT)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps FA") || message.equalsIgnoreCase("p!ps Fanatic")) {
            event.getTextChannel().sendMessage((Constants.pskillFA)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps FI") || message.equalsIgnoreCase("p!ps Firmness")) {
            event.getTextChannel().sendMessage((Constants.pskillFI)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps FU") || message.equalsIgnoreCase("p!ps Follow Up")) {
            event.getTextChannel().sendMessage((Constants.pskillFU)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps FD") || message.equalsIgnoreCase("p!ps Frost Diver")) {
            event.getTextChannel().sendMessage((Constants.pskillFD)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps GAS") || message.equalsIgnoreCase("p!ps Group Assassination")) {
            event.getTextChannel().sendMessage((Constants.pskillGAS)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps GAW") || message.equalsIgnoreCase("p!ps Group Awaken")) {
            event.getTextChannel().sendMessage((Constants.pskillGAW)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps GC") || message.equalsIgnoreCase("p!ps Group Counter")) {
            event.getTextChannel().sendMessage((Constants.pskillGC)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps GF") || message.equalsIgnoreCase("p!ps Group Freeze")) {
            event.getTextChannel().sendMessage((Constants.pskillGF)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps GR") || message.equalsIgnoreCase("p!ps Group Restore")) {
            event.getTextChannel().sendMessage((Constants.pskillGR)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps GSL") || message.equalsIgnoreCase("p!ps Group Sleep")) {
            event.getTextChannel().sendMessage((Constants.pskillGSL)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps GSO") || message.equalsIgnoreCase("p!ps Group Stone")) {
            event.getTextChannel().sendMessage((Constants.pskillGSO)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps GSR") || message.equalsIgnoreCase("p!ps Group Strengthen")) {
            event.getTextChannel().sendMessage((Constants.pskillGSR)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps GSU") || message.equalsIgnoreCase("p!ps Group Stun")) {
            event.getTextChannel().sendMessage((Constants.pskillGSU)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps LS") || message.equalsIgnoreCase("p!ps Life Stealer")) {
            event.getTextChannel().sendMessage((Constants.pskillLS)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps LT") || message.equalsIgnoreCase("p!ps Life Transfer")) {
            event.getTextChannel().sendMessage((Constants.pskillLT)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps MB") || message.equalsIgnoreCase("p!ps Magic Burn")) {
            event.getTextChannel().sendMessage((Constants.pskillMB)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps MR") || message.equalsIgnoreCase("p!ps Marker")) {
            event.getTextChannel().sendMessage((Constants.pskillMR)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps MA") || message.equalsIgnoreCase("p!ps DA") || message.equalsIgnoreCase("p!ps Multiple Attack")) {
            event.getTextChannel().sendMessage((Constants.pskillMA)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps PR") || message.equalsIgnoreCase("p!ps Protection")) {
            event.getTextChannel().sendMessage((Constants.pskillPR)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps QS") || message.equalsIgnoreCase("p!ps Quick Spell")) {
            event.getTextChannel().sendMessage((Constants.pskillQS)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps RA") || message.equalsIgnoreCase("p!ps Raid Away")) {
            event.getTextChannel().sendMessage((Constants.pskillRA)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps RE") || message.equalsIgnoreCase("p!ps Restore")) {
            event.getTextChannel().sendMessage((Constants.pskillRE)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps ST") || message.equalsIgnoreCase("p!ps Shield of Tardy")) {
            event.getTextChannel().sendMessage((Constants.pskillST)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps SL") || message.equalsIgnoreCase("p!ps Sleep")) {
            event.getTextChannel().sendMessage((Constants.pskillSL)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps SA") || message.equalsIgnoreCase("p!ps Sleep Attack")) {
            event.getTextChannel().sendMessage((Constants.pskillSA)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps SN") || message.equalsIgnoreCase("p!ps Sneer")) {
            event.getTextChannel().sendMessage((Constants.pskillSN)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps SD") || message.equalsIgnoreCase("p!ps Soul Destroyer")) {
            event.getTextChannel().sendMessage((Constants.pskillSD)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps SCO") || message.equalsIgnoreCase("p!ps Spirit Counter")) {
            event.getTextChannel().sendMessage((Constants.pskillSCO)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps SCU") || message.equalsIgnoreCase("p!ps Stone Curse")) {
            event.getTextChannel().sendMessage((Constants.pskillSCU)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps SR") || message.equalsIgnoreCase("p!ps Strengthen")) {
            event.getTextChannel().sendMessage((Constants.pskillSR)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps SW") || message.equalsIgnoreCase("p!ps Swift")) {
            event.getTextChannel().sendMessage((Constants.pskillSW)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps TH") || message.equalsIgnoreCase("p!ps Thump")) {
            event.getTextChannel().sendMessage((Constants.pskillTH)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps UN") || message.equalsIgnoreCase("p!ps Unyield")) {
            event.getTextChannel().sendMessage((Constants.pskillUN)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        } else if (message.equalsIgnoreCase("p!ps WT") || message.equalsIgnoreCase("p!ps Weak Track")) {
            event.getTextChannel().sendMessage((Constants.pskillWT)).queue((message2 -> message2.delete().queueAfter(240, TimeUnit.SECONDS)));
            counter++;
        }
        return counter;
    }
}
