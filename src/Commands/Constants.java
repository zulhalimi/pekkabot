package Commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.awt.*;
import java.util.concurrent.TimeUnit;

public class Constants {
    public static final String discordToken = "NDI4MTc2NDY4NDcyODg5MzQ1.DZvTeA.0Sbk74cxOygihHBBpTkjHRBqKD4"; /*Insert Your Own Discord Token*/

    public static final String whale = "\uD83D\uDC20\n" +
            "\uD83D\uDC1F\n" +
            "\uD83D\uDC2C\n" +
            "\uD83D\uDC0B\n" +
            "\uD83D\uDC27\n" +
            "\uD83C\uDF0C\uD83D\uDC33";
    public static final String help = "```Apache\n" +
            "[Command]      : [Description]\n" +
            "hello          : Say hello\n" +
            "rebirth        : Lists the rebirth info\n" +
            "pet            : List the pets ID Card\n" +
            "skill          : Lists skill info\n" +
            "sHonor         : Lists skill honor\n" +
            "pMonth         : Pet Shards per Month\n" +
            "pStat          : MVP Stats\n" +
            "ps         : Combat Skills\n" +
            "rune           : Runes\n" +
            "petxp          : Explore Pet XP\n" +
            "iam            : Use m/s/a\n" +
            "spreadsheet    : Link to the spreadsheet\n" +
            "calc           : Link to damage calculator\n" +
            "addMe          : Link to add PekkaBot\n" +
            "server         : Link to PekkaBot official server```";

    public static final String emojiList= "<:petme:392210462747525120>: profile\n" +
            "<:dangoP:392215978752344064>: dangoP\n" +
            "<:dangoB:392215978685235200>: dangoB\n" +
            "<:dangoY:392215978760470528>: dangoY\n";

    public static final String aSpreadsheet = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-4P6apxAjc_zTv_biBf7JyUto3anf2GpBAkkpysXA9h2woNfMxSCUB_E1OrEcJ5LI4iszptdEHf0x/pubhtml";
    public static final String pSpreadsheet = "https://docs.google.com/spreadsheets/d/1f70tFOsNTzVSdCXgn-EKw-B6zBXyviMo6uWVAXzVp34/edit?usp=sharing";

    public static final String rebirth2 = "```md\n" +
            "RB2 Requirements\n" +
            "\n" +
            "MVP tank (33++ shards, the more the better)\n" +
            "Asthayi 3\n" +
            "Savage (combat ready w/ 5 shards)\n" +
            "Pirate Skeleton (combat ready w/ 5 shards)\n" +
            "Archer Skeleton (combat ready w/ 5 shards)\n" +
            "Bigfoot/Hornet/Farmiliar/Eclipse (combat ready w/ 5 shards,\n" +
            "choose 1)\n" +
            "350k Smelt pts.\n" +
            "Honor Level 39\n" +
            "Level 100 damage explore pets```";
    public static final String rebirth3 = "```md\n" +
            "RB3 Requirements\n" +
            "\n" +
            "**for PVE**\n" +
            "Str 2/3\n" +
            "Grp. Str 2/3\n" +
            "Restore 2/3\n" +
            "Grp. Resore 2/3\n" +
            "1 CC3 1 CC2\n" +
            "QS 2/3\n" +
            "\n" +
            "or\n" +
            "\n" +
            "WT3\n" +
            "Swift\n" +
            "Marker\n" +
            "Protection3\n" +
            "\n" +
            "**for PVP prepare**\n" +
            "CC3 (Group Sleep)\n" +
            "Unyield3\n" +
            "Protection3\n" +
            "Str3\n" +
            "Firmness3\n" +
            "QS3\n" +
            "Restore3\n" +
            "Cure3/Grp.Restore3 (pick 1)\n" +
            "Counter3/Grp. Counter3 (pick 1)";

    public static final String petMonth = "```Apache\n" +
            "[Month]    | [Pet]\n" +
            "January    | Deviling\n" +
            "Febuary    | Bigfoot\n" +
            "March      | Jakk\n" +
            "April      | Deniro\n" +
            "May        | Ghostring\n" +
            "June       | High Orc\n" +
            "July       | King Goblin\n" +
            "August     | Rotar Zairo\n" +
            "September  | Angeling\n" +
            "October    | Steam Goblin\n" +
            "November   | Isis\n" +
            "December   | Smokie```";

    public static final String sHonorListHelp = "```Apache\n" +
            "sHonor 0     : Level 1-39\n" +
            "sHonor 40    : Level 40-64```";

    public static final String Mindblow = "```Mindblow / Bash / Brutal Arrow```";
    public static final String MagicalShock = "```md\nH1/H7\n\nFatal Blow / Power Arrow / Magical Shock / Coma\n\n" +
            "<Effect: Deals damage and stuns the opponent.>\n" +
            "Lvl 2 - Prolong critical duration.```";
    public static final String Berserk = "```md\nH2/H10\n\nBerserk / Fury Concentration\n\n" +
            "<Effect: Greatly increases the critical strike rate for a short period.>\n" +
            "Lvl 2 - Prolong critical duration.```";
    public static final String MagicalDouble = "```md\nH3/H13\n\nMagical Double Hit / Multiple Attack / Double Strafe / Assasination\n\n" +
            "<Effect: Attacks the opponent twice with quick succession and great damage.>\n" +
            "Lvl 2 - Increases the damage dealt by Double Attack I/Double Shot I whilst also adding a healing effect to each attack.```";
    public static final String StoneCurse = "```md\nH4/H16\n\nStone Curse / Frost Blade / Frost Bolt / Frost Assasination\n\n" +
            "<Effect: Deals moderate damage and then stuns the enemy briefly.>\n" +
            "Lvl 2 - Improves the damage inflicted.```";
    public static final String Provoke = "```md\nH5/H19\n\nProvoke / Enchant Poison\n\n" +
            "<Effect: Lowers the DEF and M.DEF for a moderate period.>\n" +
            "Lvl 2 - Greatly reduces the M.DEF and DEF of the enemy.```";
    public static final String Sleep = "```md\nH6/H21\n\nSleep / Sleeping Blade / Sleep Bolt / Sleeping Strike\n\n" +
            "<Effect: Strikes very quickly, deals a low amount of damage and stuns the enemy for one turn.>\n" +
            "Lvl 2 - Greatly reduces the M.DEF and DEF of the enemy.```";
    public static final String CounterAttack = "```md\nH8/H22\n\nCounter Attack\n\n" +
            "<Effect: Creates a shield around the player that slightly\n" +
            "reduces incoming damage and reflects a portion of incoming\n" +
            "damage back to the enemy.>\n" +
            "Lvl 2 - Increases the amount of damage reflected slightly and\n" +
            "adds a chance to stun when attacked.```";
    public static final String Cure = "```md\nH9/H24\n\nCure\n\n" +
            "<Effect: Invokes a single-turn heal, power varies depending\n" +
            "on the % of health the character is missing.>\n" +
            "Lvl 2 - Moderately increases the factor of healing and adds\n" +
            "a 10% of healed amount to heal over time.```";
    public static final String Silence = "```md\nH11/H26\n\nSilence / Blade of Silence / Silence Bolt / Silence Blade\n\n" +
            "<Effect: Silences the enemy for two turns, preventing them\n" +
            "from casting any abilities.>\n" +
            "Lvl 2 - Moderately increases the damage dealt and increases \n" +
            "the duration of the silent debuff.```";
    public static final String ElementFocus = "```md\nH12/H27\n\nElement Focus / Spear Dynamo / Meditation / Spear Dynamo\n\n" +
            "<Effect: Greatly increases the HIT rate (accuracy) of the\n" +
            "character.>\n" +
            "Lvl 2 - Greatly increases the character's hit rate (accuracy).\n" +
            "It lasts for a long period.```";
    public static final String Dexterity = "```md\nH14/H28\n\nDexterity / Dexterity / Wind Walker" +
            "<Effect: Greatly increases the character's evasion, causing\n" +
            "the enemy to miss attacks.>\n" +
            "Lvl 2 - Increase EVA and become immune to harmful skills.\n" +
            "It lasts for a long period.```";
    public static final String MentalPower = "```md\nH15/H31\n\nMental Power / Triple Action / Triple Strafe / Double Attack\n\n" +
            "<Effect: Strikes three times in quick succession with moderate\n" +
            "power.>\n" +
            "Lvl 2 - Triple Action: Increases the damage dealt by this skill\n" +
            "and adds bonus damage dependent on the % of HP the\n" +
            "character is currently at (higher % deals more damage).\n" +
            "Triple Strafe: Increases the damage and critical chance of this\n" +
            "skill.\n" +
            "Mental Power: Each hit may stun the enemies.\n" +
            "Double Attack: Can reduce the target's attack.```";
    public static final String Awaken = "```md\nH17/H33\n\nAwaken\n\n" +
            "<Effect: Over the duration of one turn recover a portion of lost\n" +
            "SP.>\n" +
            "Lvl 2 - Restores a larger amount of SP than the previous tier\n" +
            "whilst also restoring an additional amount over time.```";
    public static final String CurseMagic = "```md\nH18/H35\n\nCurse Magic / Cursed Blade / Cursed Bolt / Cursed Strike\n\n" +
            "<Effect: Deals a moderate amount of damage to the enemy\n" +
            "and lowers their chance to land attacks and resist status debuffs.>\n" +
            "Lvl 2 - Increases the damage dealt and duration of the debuff\n" +
            "applied by the previous tier of this skill.```";
    public static final String SpeedBurst = "```md\nH20/H38\n\nSpeed Burst / Swift\n\n" +
            "<Effect: Increases the character's attack speed greatly.>\n" +
            "Lvl 2 - Greatly increases the attack speed of the character,\n" +
            "even more so than the previous tier of this skill.```";
    public static final String EnergyReflection = "```md\nH23/H41\n\nEnergy Reflection / Furious Blow / Furry Arrow / Slash Attack\n\n" +
            "<Effect: Channels for a long period, at the end of the channel \n" +
            "period deals a large amount of damage based on the amount of\n" +
            "damage taken whilst channeling this skill.>\n" +
            "Lvl 2 - Same effects as previous tier but with an added heal after\n" +
            "casting is complete.```";
    public static final String AgiReduction = "```md\nH25/H43\n\nAgi Reduction\n\n" +
            "<Effect: Greatly decreases the agility of the enemy, slowing their\n" +
            "attacks and ability casts.>\n" +
            "Lvl 2 - Becomes AOE and increases the amount of SP needed \n" +
            "by the enemy to cast abilities. Advanced Reduce, affects all \n" +
            "opponents.```";
    public static final String TargetChange = "```md\nH29/H37\n\nTarget Change\n\n" +
            "<Effect: Draw all enemies to attack yourself and it lasts for a short\n" +
            "time.>\n";
    public static final String Concentration = "```md\nH30/H45\n\nConcentration\n\n" +
            "<Effect: Reduce the duration of debuffs inflicted upon the character.>\n" +
            "Lvl 2 - Makes the character completely immune to status effects \n" +
            "for a period of time.```";
    public static final String ElementBomb = "```md\nH32/H46\n\nElement Bomb / Charged Attack / Aimed Bolt / Silent Assassin\n\n" +
            "<Effect/Lvl 2: Sword: Deals huge damage to the enemy and restore \n" +
            "HP\n" +
            "Archer/Mage: Deals massive damage, increases HIT chance\n" +
            "Thief: Cannot be targeted by single target skills during cast.>```";
    public static final String ThunderousShot = "```md\nH34/H47\n\nThunderous Shot / Vital Strike / Falcon Eyes / Grimtooth\n\n" +
            "<Effect: Makes the enemy more vulnerable to critical strikes if the \n" +
            "enemy is currently debuffed.>\n" +
            "Lvl 2 - Increases the damage dealt by this skill and causes the \n" +
            "opponent to take additional damage when attacked, and inflict \n" +
            "bleeding.```";
    public static final String FireWall = "```md\nH36/H49\n\nFire Wall / Pierce / Ghost Arrow / Cross Impact\n\n" +
            "<Effect: Single target skill that deals three attacks in succession over \n" +
            "three turns, each attack dealing more damage than the last.>\n" +
            "Lvl 2 - Has a chance of silence on 2nd and 3rd hit, can be dodged.```";
    public static final String MagicalChase = "```md\nH39/H50\n\nMagical Chase / Traumatic Blow / Vulnerability Mark / Exploit Weakness\n\n" +
            "<Effect: Damage increases with every critical strike.>\n" +
            "Lvl 2 - Sword: Crit will reduce enemy's def.\n" +
            "Archer: Crit will increase attack speed.\n" +
            "Mage: Crit will curse the enemy.\n" +
            "Thief: Normal attack gains damage.```";
    public static final String EnergyTransfer = "```md\nH40/H51\n\nEnergy Transfer / Blade of Blood / Blood Steal / Bloody Katar\n\n" +
            "<Effect: Deals damage to the enemy and restores health dependent \n" +
            "on the damage dealt.>\n" +
            "Lvl 2 - Sword: Deals increased damage to the enemy and restores \n" +
            "health, has increased restored HP.\n" +
            "Archer: Deals increased damage to the enemy and restores health \n" +
            "dependent on the damage dealt, critical will cause bleeding.\n" +
            "Mage: Deals increased damage to the enemy and restores health, \n" +
            "when recovering HP, will also recovers 30% as SP.\n" +
            "Thief: Increase Crit Chance.```";
    public static final String Distraction = "```md\nH42/H48\n\nDistraction\n\n" +
            "<Effect: Attacks the enemy, increases the cast time of the enemy and \n" +
            "increases the amount of SP needed by the enemy to cast abilities.>\n" +
            "Lvl 2 - Becomes AOE.```";
    public static final String Clearance = "```md\nH44/H52\n\nClearance\n\n" +
            "<Effect: Makes the character completely immune to status effects for \n" +
            "a period of time.>\n" +
            "Lvl 2 - Carries all of the effects of the previous level of this skill whilst \n" +
            "also inflicting curse upon the enemy if this skill successfully lands.```";
    public static final String HiddenPotential = "```md\nH53/H61\n\nHidden Potential\n\n" +
            "<Effect: Attack Enemy, the longer the duration of the fight, the higher \n" +
            "is the damage dealt.>```";
    public static final String PowerImpact = "```md\nH54/H57\n\nPower Impact\n\n" +
            "<Effect: Attack the Opponent, Attacks more times, based on the \n" +
            "enemy's HP.>\n" +
            "Lvl 2 - Increased number of attacks.```";
    public static final String PowerShield = "```md\nH55/H59\n\nPower Shield\n\n" +
            "<Effect: During certain period delay the damage taken to affect you.\n" +
            "Lvl 2 - Will resist part of the damage.>```";
    public static final String Mislead = "```md\nH56/H63\n\nMislead\n\n" +
            "<Effect: Mislead the Opponent. Causing his skills to Affect himself too.>\n" +
            "Lvl 2 - Opponent will take full damage.```";
    public static final String BattleChains = "```md\nH58/H64\n\nBattle Chains\n\n" +
            "<Effect: Increase ATK and DEF based on the Combat Pet Attributes.>\n" +
            "Lvl 2 - Also increase other member's Attributes.```";
    public static final String Intimidation = "```md\nH60/H62\n\nIntimidation\n\n" +
            "<Effect: During Certain period, critical attacks will weaken the enemy, \n" +
            "every 5 critical hits, the effect will increase.>\n" +
            "Lvl 2 - During Certain period, critical attacks will weaken the enemy, \n" +
            "every 5 critical hits, the effect will increase, decrease enemy's cast and \n" +
            "attack speed.```";
    public static final String EnergyCoat = "```Energy Coat / Endure / Guardian```";

    public static final String sHonorListM0 = "```Apache\n" +
            "|Lv|Total |Next |Skill Name           |\n" +
            "|1 |0     |200  |Magical Shock I      |\n" +
            "|2 |200   |300  |Berserk I            |\n" +
            "|3 |500   |500  |Magical Double Hit I |\n" +
            "|4 |1000  |500  |Stone Curse I        |\n" +
            "|5 |1500  |500  |Provoke I            |\n" +
            "|6 |2000  |1000 |Sleep I              |\n" +
            "|7 |3000  |1000 |Magical Shock II     |\n" +
            "|8 |4000  |1000 |Counter Attack I     |\n" +
            "|9 |5000  |1000 |Cure I               |\n" +
            "|10|6000  |2500 |Berserk I            |\n" +
            "|11|8500  |2100 |Silence I            |\n" +
            "|12|10600 |2300 |Element Focus I      |\n" +
            "|13|12900 |2600 |Magical Double Hit II|\n" +
            "|14|15500 |2800 |Dexterity I          |\n" +
            "|15|18300 |3200 |Mental Power I       |\n" +
            "|16|21500 |3400 |Stone Curse II       |\n" +
            "|17|24900 |3700 |Awaken I             |\n" +
            "|18|28600 |4100 |Curse Magic I        |\n" +
            "|19|32700 |4300 |Provoke II           |\n" +
            "|20|37000 |4800 |Speed Burst I        |\n" +
            "|21|41800 |5000 |Sleep II             |\n" +
            "|22|46800 |5500 |Counter Attack II    |\n" +
            "|23|52300 |5800 |Energy Reflection I  |\n" +
            "|24|58100 |6200 |Cure II              |\n" +
            "|25|64300 |6600 |AGI Reduction I      |\n" +
            "|26|70900 |7000 |Silence II           |\n" +
            "|27|77900 |7400 |Element Focus II     |\n" +
            "|28|85300 |4700 |Dexterity II         |\n" +
            "|29|90000 |3100 |Target Change I      |\n" +
            "|30|93100 |8300 |Concentration I      |\n" +
            "|31|101400|8800 |Mental Power II      |\n" +
            "|32|110200|11600|Element Bomb I       |\n" +
            "|33|121800|7200 |Awaken II            |\n" +
            "|34|129000|10200|Thunderous Shot I    |\n" +
            "|35|139200|10700|Curse Magic II       |\n" +
            "|36|149900|100  |Fire Wall I          |\n" +
            "|37|150000|11000|Target Change II     |\n" +
            "|38|161000|11700|Speed Burst II       |\n" +
            "|39|172700|12200|Magical Chase I      |```";
    public static final String sHonorListS0 = "```Apache\n" +
            "|Lv|Total |Next |Skill Name           |\n" +
            "|1 |0     |200  |Fatal Blow I         |\n" +
            "|2 |200   |300  |Berserk I            |\n" +
            "|3 |500   |500  |Multiple Attack I    |\n" +
            "|4 |1000  |500  |Frost Blade I        |\n" +
            "|5 |1500  |500  |Provoke I            |\n" +
            "|6 |2000  |1000 |Sleeping Blade I     |\n" +
            "|7 |3000  |1000 |Fatal Blow II        |\n" +
            "|8 |4000  |1000 |Counter Attack I     |\n" +
            "|9 |5000  |1000 |Cure I               |\n" +
            "|10|6000  |2500 |Berserk I            |\n" +
            "|11|8500  |2100 |Blade of Silence     |\n" +
            "|12|10600 |2300 |Spear Dynamo  I      |\n" +
            "|13|12900 |2600 |Multiple Attack II   |\n" +
            "|14|15500 |2800 |Dexterity I          |\n" +
            "|15|18300 |3200 |Triple Action I      |\n" +
            "|16|21500 |3400 |Frost Blade II       |\n" +
            "|17|24900 |3700 |Awaken I             |\n" +
            "|18|28600 |4100 |Cursed Blade I       |\n" +
            "|19|32700 |4300 |Provoke II           |\n" +
            "|20|37000 |4800 |Speed Burst I        |\n" +
            "|21|41800 |5000 |Sleeping Blade II    |\n" +
            "|22|46800 |5500 |Counter Attack II    |\n" +
            "|23|52300 |5800 |Furious Blow I       |\n" +
            "|24|58100 |6200 |Cure II              |\n" +
            "|25|64300 |6600 |AGI Reduction I      |\n" +
            "|26|70900 |7000 |Blade of Silence II  |\n" +
            "|27|77900 |7400 |Spear Dynamo II      |\n" +
            "|28|85300 |4700 |Dexterity II         |\n" +
            "|29|90000 |3100 |Target Change I      |\n" +
            "|30|93100 |8300 |Concentration I      |\n" +
            "|31|101400|8800 |Triple Action II     |\n" +
            "|32|110200|11600|Charged Attack I     |\n" +
            "|33|121800|7200 |Awaken II            |\n" +
            "|34|129000|10200|Vital Strike I       |\n" +
            "|35|139200|10700|Cursed Blade II      |\n" +
            "|36|149900|100  |Pierce I             |\n" +
            "|37|150000|11000|Target Change II     |\n" +
            "|38|161000|11700|Speed Burst II       |\n" +
            "|39|172700|12200|Traumatic Blow I     |```";
    public static final String sHonorListA0 = "```Apache\n" +
            "|Lv|Total |Next |Skill Name           |\n" +
            "|1 |0     |200  |Brutal Arrow I       |\n" +
            "|2 |200   |300  |Berserk I            |\n" +
            "|3 |500   |500  |Double Strafe I      |\n" +
            "|4 |1000  |500  |Frost Bolt I         |\n" +
            "|5 |1500  |500  |Provoke I            |\n" +
            "|6 |2000  |1000 |Sleep Bolt I         |\n" +
            "|7 |3000  |1000 |Brutal Arrow II      |\n" +
            "|8 |4000  |1000 |Counter Attack I     |\n" +
            "|9 |5000  |1000 |Cure I               |\n" +
            "|10|6000  |2500 |Berserk I            |\n" +
            "|11|8500  |2100 |Silence Bolt I       |\n" +
            "|12|10600 |2300 |Meditation I         |\n" +
            "|13|12900 |2600 |Double Strafe II     |\n" +
            "|14|15500 |2800 |Windwalker I         |\n" +
            "|15|18300 |3200 |Triple StrafeI       |\n" +
            "|16|21500 |3400 |Frost Bolt II        |\n" +
            "|17|24900 |3700 |Awaken I             |\n" +
            "|18|28600 |4100 |Curse Bolt I         |\n" +
            "|19|32700 |4300 |Provoke II           |\n" +
            "|20|37000 |4800 |Speed Burst I        |\n" +
            "|21|41800 |5000 |Sleep Bolt II        |\n" +
            "|22|46800 |5500 |Counter Attack II    |\n" +
            "|23|52300 |5800 |Furry Arrow I        |\n" +
            "|24|58100 |6200 |Cure II              |\n" +
            "|25|64300 |6600 |AGI Reduction I      |\n" +
            "|26|70900 |7000 |Silence Bolt II      |\n" +
            "|27|77900 |7400 |Furry Arrow II       |\n" +
            "|28|85300 |4700 |Windwalker II        |\n" +
            "|29|90000 |3100 |Target Change I      |\n" +
            "|30|93100 |8300 |Concentration I      |\n" +
            "|31|101400|8800 |Triple Strafe II     |\n" +
            "|32|110200|11600|Aimed Bolt I         |\n" +
            "|33|121800|7200 |Awaken II            |\n" +
            "|34|129000|10200|Falcon Eyes I        |\n" +
            "|35|139200|10700|Curse Bolt  II       |\n" +
            "|36|149900|100  |Ghost Arrow I        |\n" +
            "|37|150000|11000|Target Change II     |\n" +
            "|38|161000|11700|Speed Burst II       |\n" +
            "|39|172700|12200|Vulnerable Mark I    |```";
    public static final String sHonorListM40 = "```Apache\n" +
            "|Lv|Total |Next |Skill Name           |\n" +
            "|40|184900|12700|Energy Transfer I    |\n" +
            "|41|197600|13300|Energy Reflection II |\n" +
            "|42|210900|13900|Distraction I        |\n" +
            "|43|224800|14400|AGI Reduction II     |\n" +
            "|44|239200|15000|Clearance I          |\n" +
            "|45|254200|15600|Concentration II     |\n" +
            "|46|269800|16200|Element Bomb II      |\n" +
            "|47|286000|16800|Thunderous Shot II   |\n" +
            "|48|302800|17500|Distraction II       |\n" +
            "|49|320300|18100|Fire Wall II         |\n" +
            "|50|338400|18700|Magical Chase II     |\n" +
            "|51|357100|     |Energy Transfer II   |\n" +
            "|52|      |     |Clearance II         |\n" +
            "|53|      |     |Hidden Potential I   |\n" +
            "|54|      |     |Power Impact I       |\n" +
            "|55|      |     |Power Shield I       |\n" +
            "|56|      |     |Mislead I            |\n" +
            "|57|      |     |Power Impact II      |\n" +
            "|58|      |     |Battle Chains I      |\n" +
            "|59|      |     |Power Shield II      |\n" +
            "|60|      |     |Intimidation I       |\n" +
            "|61|      |     |Hidden Potential II  |\n" +
            "|62|      |     |Intimidation II      |\n" +
            "|63|      |     |Mislead II           |\n" +
            "|64|      |     |Battle Chains II     |```";
    public static final String sHonorListS40 = "```Apache\n" +
            "|Lv|Total |Next |Skill Name           |\n" +
            "|40|184900|12700|Blade of Blood I     |\n" +
            "|41|197600|13300|Furious Blow II      |\n" +
            "|42|210900|13900|Distraction I        |\n" +
            "|43|224800|14400|AGI Reduction II     |\n" +
            "|44|239200|15000|Clearance I          |\n" +
            "|45|254200|15600|Concentration II     |\n" +
            "|46|269800|16200|Charged Attack II    |\n" +
            "|47|286000|16800|Vital Strike II      |\n" +
            "|48|302800|17500|Distraction II       |\n" +
            "|49|320300|18100|Pierce II            |\n" +
            "|50|338400|18700|Traumatic Blow II    |\n" +
            "|51|357100|     |Blade of Blood II   |\n" +
            "|52|      |     |Clearance II         |\n" +
            "|53|      |     |Hidden Potential I   |\n" +
            "|54|      |     |Power Impact I       |\n" +
            "|55|      |     |Power Shield I       |\n" +
            "|56|      |     |Mislead I            |\n" +
            "|57|      |     |Power Impact II      |\n" +
            "|58|      |     |Battle Chains I      |\n" +
            "|59|      |     |Power Shield II      |\n" +
            "|60|      |     |Intimidation I       |\n" +
            "|61|      |     |Hidden Potential II  |\n" +
            "|62|      |     |Intimidation II      |\n" +
            "|63|      |     |Mislead II           |\n" +
            "|64|      |     |Battle Chains II     |```";
    public static final String sHonorListA40 = "```Apache\n" +
            "|Lv|Total |Next |Skill Name           |\n" +
            "|40|184900|12700|Blood Steal I        |\n" +
            "|41|197600|13300|Furry Arrow II       |\n" +
            "|42|210900|13900|Distraction I        |\n" +
            "|43|224800|14400|AGI Reduction II     |\n" +
            "|44|239200|15000|Clearance I          |\n" +
            "|45|254200|15600|Concentration II     |\n" +
            "|46|269800|16200|Aimed Bolt II        |\n" +
            "|47|286000|16800|Falcon Eyes II       |\n" +
            "|48|302800|17500|Distraction II       |\n" +
            "|49|320300|18100|Ghost Arrow II       |\n" +
            "|50|338400|18700|Vulnerability Mark II|\n" +
            "|51|357100|     |Blood Steal II       |\n" +
            "|52|      |     |Clearance II         |\n" +
            "|53|      |     |Hidden Potential I   |\n" +
            "|54|      |     |Power Impact I       |\n" +
            "|55|      |     |Power Shield I       |\n" +
            "|56|      |     |Mislead I            |\n" +
            "|57|      |     |Power Impact II      |\n" +
            "|58|      |     |Battle Chains I      |\n" +
            "|59|      |     |Power Shield II      |\n" +
            "|60|      |     |Intimidation I       |\n" +
            "|61|      |     |Hidden Potential II  |\n" +
            "|62|      |     |Intimidation II      |\n" +
            "|63|      |     |Mislead II           |\n" +
            "|64|      |     |Battle Chains II     |```";

    public static final String goblinHelp = "```Apache\n" +
            "[Command]   : [Goblin]\n" +
            "goblin 1    : Goblin :O\n" +
            "goblin 2    : Goblin -_-\n" +
            "goblin 3    : Goblin :(\n" +
            "goblin 4    : Goblin :D```";

    public static final String petSkillHelp = "```Apache\n" +
            "[Command]    :[PetSkillName]\n" +
            "ps AS    : Attack Strengthen\n" +
            "ps AT    : Attack Terminator\n" +
            "ps AW    : Awaken\n" +
            "ps BS    : Bloodshot\n" +
            "ps CC    : Concentration\n" +
            "ps CE    : Controlled Enhance\n" +
            "ps CA    : Counter Attack\n" +
            "ps CU    : Cure\n" +
            "ps DS    : DMG Share\n" +
            "ps ED    : Element Delay\n" +
            "ps FA    : Fanatic\n" +
            "ps FI    : Firmness\n" +
            "ps FU    : Follow-up\n" +
            "ps FD    : Frost Diver\n" +
            "ps GAS   : Group Assassination\n" +
            "ps GAW   : Group Awaken\n" +
            "ps GC    : Group Counter\n" +
            "ps GF    : Group Freeze\n" +
            "ps GR    : Group Restore\n" +
            "ps GSL   : Group Sleep\n" +
            "ps GSO   : Group Stone\n" +
            "ps GSR   : Group Strengthen\n" +
            "ps GSU   : Group Stun\n" +
            "ps LS    : Life Stealer\n" +
            "ps LT    : Life Transfer\n" +
            "ps MB    : Magic Burn\n" +
            "ps MR    : Marker\n" +
            "ps MA    : Multiple Attack\n" +
            "ps PR    : Protection\n" +
            "ps QS    : Quick Spell\n" +
            "ps RA    : Raid Away\n" +
            "ps RE    : Restore\n" +
            "ps ST    : Shield of Tardy\n" +
            "ps SL    : Sleep\n" +
            "ps SA    : Sleep Attack\n" +
            "ps SN    : Sneer\n" +
            "ps SD    : Soul Destroyer\n" +
            "ps SCO   : Spirit Counter\n" +
            "ps SCU   : Stone Curse\n" +
            "ps SR    : Strengthen\n" +
            "ps SW    : Swift\n" +
            "ps TH    : Thump\n" +
            "ps UN    : Unyield\n" +
            "ps WT    : Weak Track```";
    public static final String pskillAS = "```md\n<Skill: Attack Strengthen>\n" +
            "<Effect: Increase Group ATK>\n\n" +
            "Unlock 1: Orc Archer Lv 50\n" +
            "Unlock 2: Piere Lv 80\n" +
            "          Deniro Lv 80\n" +
            "Unlock 3: Evil Skeleton Lv 100\n" +
            "          Warrior Orc Lv 100```";
    public static final String pskillAW = "```md\n<Skill: Awaken>\n" +
            "<Effect: Group SP Restore>\n\n" +
            "Unlock 1: Muka Lv 50```";
    public static final String pskillBS = "```md\n<Skill: Bloodshot>\n" +
            "<Effect: Attack the strongest Enemy>\n\n" +
            "Unlock 1: Golden Thief Bug Lv 50\n" +
            "          OR Angeling Lv 80\n" +
            "Unlock 2: Baphomet Lv 80\n" +
            "          OR Jakk Lv 100\n" +
            "Unlock 3: Finish PvP in Masters```";
    public static final String pskillCC = "```md\n<Skill: Concentration>\n" +
            "<Effect: Restore SP to ally with lowest SP>\n\n" +
            "Unlock 1: Gargoyle Lv 50\n" +
            "Unlock 2: Munak Lv 100\n" +
            "Unlock 3: Bon Gun Lv 100\n" +
            "          Hydra Lv 100\n" +
            "Unlock 4: Marc Lv 150\n" +
            "          Muka Lv 150```";
    public static final String pskillCE = "```md\n<Skill: Controlled Enhance>\n" +
            "<Effect: CC do more DMG and increased duration>\n\n" +
            "Unlock 1: Pet Map\n" +
            "Unlock 2: Pet Map\n" +
            "Unlock 3: Pet Map```";
    public static final String pskillCA = "```md\n<Skill: Counter Attack>\n" +
            "<Effect: Reflect DMG>\n\n" +
            "Unlock 1: Yoyo Lv 50\n" +
            "Unlock 2: Goblin -.- Lv 100\n" +
            "Unlock 3: Deniro Lv 120```";
    public static final String pskillCU = "```md\n<Skill: Cure>\n" +
            "<Effect: Heal weakest ally>\n\n" +
            "Unlock 1: Archer Skeleton Lv 50\n" +
            "Unlock 2: Evil Skeleton Lv 100\n" +
            "Unlock 3: Poporing Lv 100\n" +
            "          Poring Lv 100\n" +
            "Unlock 4: Rebirth 4```";
    public static final String pskillDS = "```md\n<Skill: Damage Share>\n" +
            "<Effect: Split DMG taken with an ally>\n\n" +
            "Unlock 1: Player Lv 110\n" +
            "Unlock 2: Andre Lv 80\n" +
            "          Viata Lv 80\n" +
            "Unlock 3: Poporing Lv 100\n" +
            "          Hornet Lv 100```";
    public static final String pskillED = "```md\n<Skill: Element Delay>\n" +
            "<Effect: Decrease enemy CAST>\n\n" +
            "Unlock 1: Pet Map\n" +
            "Unlock 2: Pet Map\n" +
            "Unlock 3: Pet Map```";
    public static final String pskillAT = "```md\n<Skill: Attack Terminator>\n" +
            "<Effect: Attack weakest enemy>\n\n" +
            "Unlock 1: Baphomet Jr. Lv 35\n" +
            "          OR Warrior Orc Lv 60\n" +
            "Unlock 2: Baphomet Lv 100\n" +
            "          OR Jakk Lv 110\n" +
            "Unlock 3: Blue Pet Lv 100\n" +
            "Unlock 4: Baphomet Lv 150\n" +
            "          Baphomet Jr. Lv 150```";
    public static final String pskillFA = "```md\n<Skill: Fanatic>\n" +
            "<Effect: Boost CRIT chance>\n\n" +
            "Unlock 1: Poporing Lv 50\n" +
            "Unlock 2: Goblin :( Lv 100\n" +
            "Unlock 3: Orc Archer Lv 100\n" +
            "          Gargoyle Lv 100\n" +
            "Unlock 4: Pirate Skeleton Lv 150\n" +
            "          Archer Skeleton Lv 150```";
    public static final String pskillFI = "```md\n<Skill: Firmness>\n" +
            "<Effect: Becomes immune to special effects (still vulnerable to CC)>\n\n" +
            "Unlock 1: Whisper Lv 30\n" +
            "          OR Orc Baby Lv 30\n" +
            "Unlock 2: Deviling lv 100\n" +
            "Unlock 3: Ghostring Lv 100```";
    public static final String pskillFU = "```md\n<Skill: Follow-up>\n" +
            "<Effect: Attack enemy multiple times>\n\n" +
            "Unlock 1: Goblin Archer Lv 50\n" +
            "Unlock 2: Goblin :o Lv 80\n" +
            "          Goblin :D Lv 80\n" +
            "Unlock 3: Goblin :( Lv 100\n" +
            "          Andre Lv 100\n" +
            "Unlock 4: Steam Goblin Lv 150\n" +
            "          King Goblin Lv 150```";
    public static final String pskillFD = "```md\n<Skill: Frost Diver>\n" +
            "<Effect: Freeze enemy>\n\n" +
            "Unlock 1: Drainliar Lv 50\n" +
            "Unlock 2: Tarou Lv 100\n" +
            "Unlock 3: Angeling Lv 120```";
    public static final String pskillGAS = "```md\n<Skill: Group Assassination>\n" +
            "<Effect: Deal DMG to all enemies>\n\n" +
            "Unlock 1: Pet Map\n" +
            "Unlock 2: Pet Map\n" +
            "Unlock 3: Pet Map```";
    public static final String pskillGAW = "```md\n<Skill: Group Awaken>\n" +
            "<Effect: Restore SP to all allies>\n\n" +
            "Unlock 1: Warrior Orc Lv 50\n" +
            "Unlock 2: Hydra Lv 100\n" +
            "Unlock 3: Player Lv 100```";
    public static final String pskillGC = "```md\n<Skill: Group Counter>\n" +
            "<Effect: Allies reflect DMG>\n\n" +
            "Unlock 1: Bon Gun Lv 50\n" +
            "Unlock 2: Willow Lv 100\n" +
            "Unlock 3: High Orc Lv 120```";
    public static final String pskillGF = "```md\n<Skill: Group Freeze>\n" +
            "<Effect: Freeze all enemies>\n\n" +
            "Unlock 1: Bigfoot Lv 50\n" +
            "Unlock 2: Goblin :o Lv 100\n" +
            "Unlock 3: Smokie Lv 120```";
    public static final String pskillGR = "```md\n<Skill: Group Restore>\n" +
            "<Effect: Restore HP to all allies>\n\n" +
            "Unlock 1: Creamy Lv 50\n" +
            "Unlock 2: Mandragora Lv 100\n" +
            "Unlock 3: Any Pet Lv 120```";
    public static final String pskillGSL = "```md\n<Skill: Group Sleep>\n" +
            "<Effect: Sleep all enemies>\n\n" +
            "Unlock 1: Eclipse Lv 50\n" +
            "Unlock 2: Goblin :D Lv 100\n" +
            "Unlock 3: Isis Lv 120```";
    public static final String pskillGSO = "```md\n<Skill: Group Stone>\n" +
            "<Effect: Petrify all enemies>\n\n" +
            "Unlock 1: Hornet Lv 50\n" +
            "Unlock 2: Vadon Lv 100\n" +
            "Unlock 3: Rotar Zairo Lv 120```";
    public static final String pskillGSR = "```md\n<Skill: Group Strengthen>\n" +
            "<Effect: Raise defense of all allies>\n\n" +
            "Unlock 1: Pirate Skeleton Lv 50\n" +
            "Unlock 2: Andre Lv 100\n" +
            "Unlock 3: Drops Lv 100\n" +
            "          Argiope Lv 100```";
    public static final String pskillGSU = "```md\n<Skill: Group Stun>\n" +
            "<Effect: Stuns all enemies>\n\n" +
            "Unlock 1: Farmiliar Lv 50\n" +
            "Unlock 2: Rocker Lv 100\n" +
            "Unlock 3: Jakk Lv 120```";
    public static final String pskillLS = "```md\n<Skill: Life Stealer>\n" +
            "<Effect: Life steal>\n\n" +
            "Unlock 1: Player Lv100\n" +
            "Unlock 2: Munak Lv 80\n" +
            "          Farmiliar Lv 80\n" +
            "Unlock 3: Hydra Lv 100\n" +
            "          Deviruchi Lv 100```";
    public static final String pskillLT = "```md\n<Skill: Life Transfer>\n" +
            "<Effect: Restore your weakest ally with part of your HP>\n\n" +
            "Unlock 1: Player Lv 60\n" +
            "Unlock 2: Spore Lv 80\n" +
            "          Eclipse Lv 80\n" +
            "Unlock 3: Mandagora Lv 100\n" +
            "          Farmiliar Lv 100```";
    public static final String pskillMB = "```md\n<Skill: Magic Burn>\n" +
            "<Effect: Make the enemy burn huge amounts of SP>\n\n" +
            "Unlock 1: Pet Map\n" +
            "Unlock 2: Pet Map\n" +
            "Unlock 3: Pet Map```";
    public static final String pskillMR = "```md\n<Skill: Marker>\n" +
            "<Effect: Marks an enemy and all allies with attack the target>\n\n" +
            "Unlock 1: Viata Lv 50\n" +
            "Unlock 2: Whisper Lv 100\n" +
            "Unlock 3: Tarou Lv 120```";
    public static final String pskillMA = "```md\n<Skill: Multiple Attack>\n" +
            "<Effect: Attack enemy multiple times>\n\n" +
            "Unlock 1: Green Pet Lv 30\n" +
            "Unlock 2: Blue Pet Lv 80\n" +
            "Unlock 3: Purple Lv 100\n" +
            "Unlock 4: Orange Pet Lv 150```";
    public static final String pskillPR = "```md\n<Skill: Protection>\n" +
            "<Effect: Increase weakest allys DEF/MDEF>\n\n" +
            "Unlock 1: Finish PvP above Silver\n" +
            "Unlock 2: Vadon Lv 80\n" +
            "          Thara Frog Lv 80\n" +
            "Unlock 3: Thief Bug Lv 100\n" +
            "          Creamy Lv 100\n" +
            "Unlock 4: Orange Pet Lv 150```";
    public static final String pskillQS = "```md\n<Skill: Quick Spell>\n" +
            "<Effect: Increase CAST>\n\n" +
            "Unlock 1: Creamy Lv 30\n" +
            "          Bon Gun Lv 30\n" +
            "Unlock 2: Drake Lv 100\n" +
            "          OR Deviling Lv 110\n" +
            "Unlock 3: Hydra Lv 100\n" +
            "          Yoyo Lv 100```";
    public static final String pskillRA = "```md\n<Skill: Raid Away>\n" +
            "<Effect: Stuns all enmies>\n\n" +
            "Unlock 1: Any Pet Lv 30\n" +
            "Unlock 2: Any Pet Lv 80\n" +
            "Unlock 3: Any Pet Lv 100```";
    public static final String pskillRE = "```md\n<Skill: Restore>\n" +
            "<Effect: Heals all allies>\n\n" +
            "Unlock 1: Drops Lv 50\n" +
            "Unlock 2: Poring Lv 100\n" +
            "Unlock 3: Lunatic Lv 100\n" +
            "          Eclipse Lv 100```";
    public static final String pskillST = "```md\n<Skill: Shield of Tardy>\n" +
            "<Effect: Lowers enemy ASD when you take DMG>\n\n" +
            "Unlock 1: Deviruchi Lv 30\n" +
            "          Deviling Lv 30\n" +
            "Unlock 2: Maya Lv 100\n" +
            "          OR Steam Goblin Lv 110\n" +
            "Unlock 3: Mistress Lv 120\n" +
            "          OR Ghostring Lv130```";
    public static final String pskillSL = "```md\n<Skill: Sleep>\n" +
            "<Effect: Sleeps enemy>\n\n" +
            "Unlock 1: Deviruchi Lv 50\n" +
            "Unlock 2: Piere Lv 100\n" +
            "Unlock 3: Deviling Lv 120```";
    public static final String pskillSA = "```md\n<Skill: Sleep Attack>\n" +
            "<Effect: Sleeps all enemies>\n\n" +
            "Unlock 1: Pet Map\n" +
            "Unlock 2: Pet Map\n" +
            "Unlock 3: Pet Map```";
    public static final String pskillSN = "```md\n<Skill: Sneer>\n" +
            "<Effect: Taunts enemies>\n\n" +
            "Unlock 1: Finish PvP above Silver\n" +
            "Unlock 2: Poring Lv 80\n" +
            "          Savage Lv 80\n" +
            "Unlock 3: Bigfoot Lv 100\n" +
            "          Orc Baby Lv 100\n" +
            "Unlock 4: Purple Pet Lv 150```";
    public static final String pskillSD = "```md\n<Skill: Soul Destroyer>\n" +
            "<Effect: Stuns enemy>\n\n" +
            "Unlock 1: Argiope Lv 50\n" +
            "Unlock 2: Fabre Lv 100\n" +
            "Unlock 3: Steam Goblin Lv 120```";
    public static final String pskillSCO = "```md\n<Skill: Spirit Counter>\n" +
            "<Effect: Reduce enemy CST when taking DMG>\n\n" +
            "Unlock 1: Whipser Lv 30\n" +
            "          Ghostring Lv 30\n" +
            "Unlock 2: Hornet Lv 100\n" +
            "          Ghostring Lv 110\n" +
            "Unlock 3: Maya Lv 120```";
    public static final String pskillSCU = "```md\n<Skill: Stone Curse>\n" +
            "<Effect: Petrify enemy>\n\n" +
            "Unlock 1: Thara Frog Lv 50\n" +
            "Unlock 2: Spore Lv 100\n" +
            "Unlock 3: King Goblin Lv 120```";
    public static final String pskillSR = "```md\n<Skill: Strengthen>\n" +
            "<Effect: Raises DEF/MDEF>\n\n" +
            "Unlock 1: Savage Lv 50\n" +
            "Unlock 2: Lunatic Lv 100\n" +
            "Unlock 3: Ghostring Lv 120\n" +
            "Unlock 4: Finish PvP in 5% Masters```";
    public static final String pskillSW = "```md\n<Skill: Swift>\n" +
            "<Effect: Increase allies ASD>\n\n" +
            "Unlock 1: Orc Baby Lv 50\n" +
            "Unlock 2: Marc Lv 100\n" +
            "Unlock 3: Steam Goblin Lv 100\n" +
            "          Rotar Zairo Lv 100```";
    public static final String pskillTH = "```md\n<Skill: Thump>\n" +
            "<Effect: Deal DMG>\n\n" +
            "Unlock 1: A Pet\n" +
            "Unlock 2: Any Pet Lv 50\n" +
            "Unlock 3: Any Pet Lv 100\n" +
            "Unlock 4: Any Pet Lv 150```";
    public static final String pskillUN = "```md\n<Skill: Unyield>\n" +
            "<Effect: Immune to CC but lower DEF/MDEF>\n\n" +
            "Unlock 1: Evil Skeleton Lv 30\n" +
            "          Archer Skeleton Lv 30\n" +
            "Unlock 2: Osiris Lv 100\n" +
            "          OR Isis Lv 110\n" +
            "Unlock 3: Goblin :D Lv 100\n" +
            "          Goblin :o Lv 100\n" +
            "Unlock 4: Deniro Lv 150\n" +
            "          Jakk lv 150```";
    public static final String pskillWT = "```md\n<Skill: Weak Track>\n" +
            "<Effect: Enemy is stunned if attacked enough>\n\n" +
            "Unlock 1: Blue Pet Lv 30\n" +
            "Unlock 2: Thief Bug Lv 80\n" +
            "          Rocker lv 80\n" +
            "Unlock 3: Fabre Lv 100\n" +
            "          Eclipse Lv 100\n" +
            "Unlock 4: Smokie Lv 150\n" +
            "          Drainliar Lv 150```";


    public static final String runeHelp = "```Apache\n" +
            "[Command]  :[RuneName]\n" +
            "rune Ad    : Adamzat\n" +
            "rune Ar    : Aries\n" +
            "rune As    : Asthayi\n" +
            "rune Ce    : Celeritas\n" +
            "rune Ci    : Ciernie\n" +
            "rune De    : Desita\n" +
            "rune Enk   : Enkeli\n" +
            "rune Enn   : Ennerwelt\n" +
            "rune Er    : Era\n" +
            "rune Fla   : Flamma\n" +
            "rune Flu   : Fluo\n" +
            "rune Ha    : Haere\n" +
            "rune Ia    : Iawanan\n" +
            "rune Iy    : Iyali\n" +
            "rune Kup   : Kupona\n" +
            "rune Kuu   : Kuura\n" +
            "rune Op    : Opari\n" +
            "rune Ta    : Taika\n" +
            "rune Vl    : Vald```";
    public static final String runeAd = "```md\n<Rune: Adamzat>\n" +
            "<Effect: Increases damage based on HP difference between character and the enemy.>\n\n" +
            "Unlock 1: Mandragora Lv 40\n" +
            "          Muka Lv 40\n" +
            "Unlock 2: Spore Lv 80\n" +
            "          Orc Baby Lv 80\n" +
            "Unlock 3: Smokie Lv 100```";
    public static final String runeAr = "```md\n<Rune: Aries>\n" +
            "<Effect: Taking DMG increases ASPD>\n\n" +
            "Unlock 1: Evil Skeleton Lv 40\n" +
            "          Orc Baby Lv 40\n" +
            "Unlock 2: Warrior Orc Lv 80\n" +
            "          Muka Lv 80\n" +
            "Unlock 3: King Goblin Lv 100```";
    public static final String runeAs = "```md\n<Rune: Asthayi>\n" +
            "<Effect: Changes skill to a double-target>\n\n" +
            "Unlock 1: Whisper Lv 40\n" +
            "          Archer Skeleton Lv 40\n" +
            "Unlock 2: Drainliar Lv 80\n" +
            "          Hornet Lv 80\n" +
            "Unlock 3: Deniro Lv 100```";
    public static final String runeCe = "```md\n<Rune: Celeritas>\n" +
            "<Effect: Increases EVA>\n\n" +
            "Unlock 1: Whisper Lv 40\n" +
            "          Marc Lv 40\n" +
            "Unlock 2: Drainliar Lv 80\n" +
            "          Yoyo Lv 80\n" +
            "Unlock 3: Ghostring Lv 100```";
    public static final String runeCi = "```md\n<Rune: Ciernie>\n" +
            "<Effect: Reflect damage>\n\n" +
            "Unlock 1: Andre Lv 40\n" +
            "          Hornet Lv 40\n" +
            "Unlock 2: Argiope Lv 80\n" +
            "          Hydra Lv 80\n" +
            "Unlock 3: Deniro Lv 100```";
    public static final String runeDe = "```md\n<Rune: Desita>\n" +
            "<Effect: Increase ATK based on buffs>\n\n" +
            "Unlock 1: Thief Bug Lv 40\n" +
            "          Viata Lv 40\n" +
            "Unlock 2: Rocker Lv 80\n" +
            "          Pirate Skeleton Lv 80\n" +
            "Unlock 3: Isis Lv 100```";
    public static final String runeEnk = "```md\n<Rune: Enkeli>\n" +
            "<Effect: Increase CRIT chance and CRITs Heal>\n\n" +
            "Unlock 1: Lunatic Lv 40\n" +
            "          Fabre Lv 40\n" +
            "Unlock 2: Tarou Lv 80\n" +
            "          Eclipse Lv 80\n" +
            "Unlock 3: Deviling Lv 100```";
    public static final String runeEnn = "```md\n<Rune: Ennerwelt>\n" +
            "<Effect: DMG decreases Enemy SP>\n\n" +
            "Unlock 1: Treasure Map\n" +
            "Unlock 2: Treasure Map\n" +
            "Unlock 3: Treasure Map```";
    public static final String runeEr = "```md\n<Rune: Era>\n" +
            "<Effect: Heal HP lost>\n\n" +
            "Unlock 1: Andre Lv 40\n" +
            "          Fabre Lv 40\n" +
            "Unlock 2: Argiope Lv 80\n" +
            "          Goblin :( Lv 80\n" +
            "Unlock 3: Isis Lv 100```";
    public static final String runeFla = "```md\n<Rune: Flamma>\n" +
            "<Effect: Attacks reduce Enemy HP and DEF/MDEF>\n\n" +
            "Unlock 1: Goblin -.- Lv 40\n" +
            "          Goblin :o Lv 40\n" +
            "Unlock 2: Goblin :D Lv 80\n" +
            "          Goblin :( Lv 80\n" +
            "Unlock 3: High Orc Lv 100```";
    public static final String runeFlu = "```md\n<Rune: Fluo>\n" +
            "<Effect: Recover HP over time>\n\n" +
            "Unlock 1: Poring Lv 40\n" +
            "          Poporing Lv 40\n" +
            "Unlock 2: Farmiliar Lv 80\n" +
            "          Munak Lv 80\n" +
            "Unlock 3: Angeling Lv 100```";
    public static final String runeHa = "```md\n<Rune: Haere>\n" +
            "<Effect: DMG beyond certain % becomes 1 for 3seconds>\n" +
            "(15%/20%/25%)\n\n" +
            "Unlock 1: Treasure Map\n" +
            "Unlock 2: Treausre Map\n" +
            "Unlock 3: Treasure Map```";
    public static final String runeIa = "```md\n<Rune: Iawanan>\n" +
            "<Effect: Becomes unkillable for a certain period of time and heal>\n\n" +
            "Unlock 1: Piere Lv 40\n" +
            "          Willow Lv 40\n" +
            "Unlock 2: Thara Frog Lv 80\n" +
            "          Drops Lv 80\n" +
            "Unlock 3: Rotar Zairo Lv 100```";
    public static final String runeIy = "```md\n<Rune: Iyali>\n" +
            "<Effect: Gain DMG for each Ally and reduce DMG taken for each Enemy>\n\n" +
            "Unlock 1: Treasure Map\n" +
            "Unlock 2: Treasure Map\n" +
            "Unlock 3: Treasure Map```";
    public static final String runeKup = "```md\n<Rune: Kupona>\n" +
            "<Effect: Reduce DMG taken>\n\n" +
            "Unlock 1: Pet Lv 30\n" +
            "Unlock 2: Pet Lv 50\n" +
            "Unlock 3: Pet Lv 100```";
    public static final String runeKuu = "```md\n<Rune: Kuura>\n" +
            "<Effect: DMG taken is converted to SP>\n\n" +
            "Unlock 1: Mandragora Lv 40\n" +
            "          Creamy Lv 40\n" +
            "Unlock 2: Spore Lv 80\n" +
            "          Bon Gun Lv 80\n" +
            "Unlock 3: Jakk Lv 100```";
    public static final String runeOp = "```md\n<Rune: Opari>\n" +
            "<Effect: Attacks deal bonus TRUE DMG>\n\n" +
            "Unlock 1: Goblin Archer Lv 40\n" +
            "          Archer Skeleton Lv 40\n" +
            "Unlock 2: Gargoyle Lv 80\n" +
            "          Orc Archer Lv 80\n" +
            "Unlock 3: Steam Goblin Lv 100```";
    public static final String runeTa = "```md\n<Rune: Taika>\n" +
            "<Effect: Cast faster and skill cannot be cancelled>\n\n" +
            "Unlock 1: Vadon Lv 40\n" +
            "          Savage Lv 40\n" +
            "Unlock 2: Bigfoot Lv 80\n" +
            "          Devilurchi Lv 80\n" +
            "Unlock 3: Smokie Lv 100```";
    public static final String runeVl = "```md\n<Rune: Vald>\n" +
            "<Effect: Increases damage and damage reduction for every attack receives>\n\n" +
            "Unlock 1: Goblin Archer Lv 40\n" +
            "          Hydra Lv 40\n" +
            "Unlock 2: Gargoyle Lv 80\n" +
            "          Drops Lv 80\n" +
            "Unlock 3: Deviling Lv 100```";

    public static final String petXP = "```Command   : Levels\n" +
            "petxp 0      : 0-19\n" +
            "petxp 20     : 20-39\n" +
            "petxp 40     : 40-59\n" +
            "petxp 60     : 60-79\n" +
            "petxp 80     : 80-100```";
    public static final String petXP0 = "```Explore XP\n" +
            "|Level|Next  |Total  |\n" +
            "|1    |10    |0      |\n" +
            "|2    |20    |10     |\n" +
            "|3    |40    |30     |\n" +
            "|4    |80    |70     |\n" +
            "|5    |150   |150    |\n" +
            "|6    |240   |300    |\n" +
            "|7    |350   |540    |\n" +
            "|8    |480   |890    |\n" +
            "|9    |630   |1370   |\n" +
            "|10   |1000  |2000   |\n" +
            "|11   |1100  |3000   |\n" +
            "|12   |1200  |4100   |\n" +
            "|13   |1300  |5300   |\n" +
            "|14   |1400  |6600   |\n" +
            "|15   |1500  |8000   |\n" +
            "|16   |1600  |9500   |\n" +
            "|17   |1700  |11100  |\n" +
            "|18   |1800  |12800  |\n" +
            "|19   |1900  |14600  |```";
    public static final String petXP20 = "`Explore XP\n" +
            "|Level|Next  |Total  |\n" +
            "|20   |2000  |16500  |\n" +
            "|21   |2200  |18500  |\n" +
            "|22   |2400  |20700  |\n" +
            "|23   |2600  |23100  |\n" +
            "|24   |2800  |25700  |\n" +
            "|25   |3000  |28500  |\n" +
            "|26   |3200  |31500  |\n" +
            "|27   |3400  |34700  |\n" +
            "|28   |3600  |38100  |\n" +
            "|29   |3800  |41700  |\n" +
            "|30   |4000  |45500  |\n" +
            "|31   |4400  |49500  |\n" +
            "|32   |4800  |53900  |\n" +
            "|33   |5200  |58700  |\n" +
            "|34   |5600  |63900  |\n" +
            "|35   |6000  |69500  |\n" +
            "|36   |6400  |75500  |\n" +
            "|37   |6800  |81900  |\n" +
            "|38   |7200  |88700  |\n" +
            "|39   |7600  |95900  |```";
    public static final String petXP40 = "```Explore XP\n" +
            "|Level|Next  |Total  |\n" +
            "|40   |8000  |103500 |\n" +
            "|41   |8800  |111500 |\n" +
            "|42   |9600  |120300 |\n" +
            "|43   |10400 |129900 |\n" +
            "|44   |11200 |140300 |\n" +
            "|45   |12000 |151500 |\n" +
            "|46   |12800 |163500 |\n" +
            "|47   |13600 |176300 |\n" +
            "|48   |14400 |189900 |\n" +
            "|49   |15200 |204300 |\n" +
            "|50   |16000 |219500 |\n" +
            "|51   |17600 |235500 |\n" +
            "|52   |19200 |253100 |\n" +
            "|53   |20800 |272300 |\n" +
            "|54   |22400 |293100 |\n" +
            "|55   |24000 |315500 |\n" +
            "|56   |25600 |339500 |\n" +
            "|57   |27200 |365100 |\n" +
            "|58   |28800 |392300 |\n" +
            "|59   |30400 |421100 |```";
    public static final String petXP60 = "```Explore XP\n" +
            "|Level|Next  |Total  |\n" +
            "|60   |23000 |451500 |\n" +
            "|61   |35200 |474500 |\n" +
            "|62   |38400 |509700 |\n" +
            "|63   |41600 |548100 |\n" +
            "|64   |44800 |589700 |\n" +
            "|65   |48000 |634500 |\n" +
            "|66   |51200 |682500 |\n" +
            "|67   |54400 |733700 |\n" +
            "|68   |57600 |788100 |\n" +
            "|69   |60800 |845700 |\n" +
            "|70   |64000 |906500 |\n" +
            "|71   |70400 |970500 |\n" +
            "|72   |76800 |1040900|\n" +
            "|73   |83200 |1117700|\n" +
            "|74   |89600 |1200900|\n" +
            "|75   |96000 |1290500|\n" +
            "|76   |102400|1386500|\n" +
            "|77   |108800|1488900|\n" +
            "|78   |115200|1597700|\n" +
            "|79   |121600|1712900|```";
    public static final String petXP80 = "```Explore XP\n" +
            "|Level|Next  |Total  |\n" +
            "|80   |128000|1834500|\n" +
            "|81   |140800|1962500|\n" +
            "|82   |153600|2103300|\n" +
            "|83   |166400|2256900|\n" +
            "|84   |179200|2423300|\n" +
            "|85   |192000|2602500|\n" +
            "|86   |204800|2794500|\n" +
            "|87   |217600|2999300|\n" +
            "|88   |230400|3216900|\n" +
            "|89   |243200|3447300|\n" +
            "|90   |256000|3690500|\n" +
            "|91   |281600|3946500|\n" +
            "|92   |307200|4228100|\n" +
            "|93   |332800|4535300|\n" +
            "|94   |358400|4868100|\n" +
            "|95   |384000|5226500|\n" +
            "|96   |409600|5610500|\n" +
            "|97   |435200|6020100|\n" +
            "|98   |460800|6455300|\n" +
            "|99   |486400|6916100|\n" +
            "|100  |0     |7402500|```";


    public static final String MVPHelp = "```Apache\n" +
            "[Command]    : [Description]\n" +
            "pStat 0     : Level 1 MVP Stats\n" +
            "pStat 30    : Level 30 MVP Stats\n" +
            "pStat 60    : Level 60 MVP Stats```";
    public static final String l1MVPL = "https://i.imgur.com/8TcuEWa.png";
    public static final String l1MVP = "```| Level 1          | HP   | SP  | ATK | MATK | DEF | MDEF | HIT | EVA | CRT  | ASD  | TEN | CAST |\n" +
            "| Baphomet         | 2475 | 295 | 288 | 288  | 150 | 150  | 66  | 66  | 85   | 93   | 66  | 66   |\n" +
            "| Baphomet Jr.     | 3217 | 295 | 249 | 249  | 195 | 195  | 85  | 66  | 66   | 66   | 66  | 66   |\n" +
            "| Dracula          | 3217 | 385 | 249 | 249  | 150 | 150  | 66  | 66  | 66   | 66   | 99  | 93   |\n" +
            "| Drake            | 3712 | 295 | 192 | 192  | 247 | 225  | 66  | 85  | 66   | 66   | 66  | 66   |\n" +
            "| Eddga            | 4083 | 295 | 192 | 192  | 225 | 225  | 66  | 85  | 66   | 66   | 66  | 66   |\n" +
            "| Golden Thief Bug | 3712 | 295 | 192 | 192  | 225 | 247  | 66  | 85  | 66   | 66   | 66  | 66   |\n" +
            "| Maya             | 3217 | 385 | 192 | 211  | 195 | 195  | 66  | 85  | 66   | 66   | 66  | 99   |\n" +
            "| Mistress         | 3217 | 422 | 249 | 249  | 150 | 150  | 66  | 66  | 66   | 66   | 99  | 85   |\n" +
            "| Moonlight        | 2788 | 256 | 215 | 215  | 168 | 168  | 74  | 57  | 57   | 57   | 57  | 62   |\n" +
            "| Osiris           | 2475 | 295 | 316 | 288  | 150 | 150  | 66  | 66  | 85   | 85   | 66  | 66   |```";
    public static final String l30MVPL = "https://i.imgur.com/zTi2D2j.png";
    public static final String l30MVP = "```| Level 30         | HP    | SP   | ATK  | MATK | DEF  | MDEF | HIT  | EVA  | CRT  | ASD  | TEN  | CAST |`\n" +
            "| Baphomet         | 35580 | 4137 | 4702 | 4702 | 2264 | 2264 | 1168 | 1168 | 1541 | 1687 | 1168 | 1168 |\n" +
            "| Baphomet Jr.     | 46652 | 4137 | 4063 | 4063 | 2959 | 2959 | 1541 | 1168 | 1168 | 1168 | 1168 | 1168 |\n" +
            "| Dracula          | 46652 | 5418 | 4063 | 4063 | 2264 | 2264 | 1168 | 1168 | 1168 | 1168 | 1791 | 1687 |\n" +
            "| Drake            | 53843 | 4137 | 3121 | 3121 | 3758 | 3421 | 1168 | 1541 | 1168 | 1168 | 1168 | 1168 |\n" +
            "| Eddga            | 59210 | 4137 | 3121 | 3121 | 3421 | 3421 | 1168 | 1541 | 1168 | 1168 | 1168 | 1168 |\n" +
            "| Golden Thief Bug | 53843 | 4137 | 3121 | 3121 | 3421 | 3758 | 1168 | 1541 | 1168 | 1168 | 1168 | 1168 |\n" +
            "| Maya             | 46652 | 5418 | 3121 | 3425 | 2959 | 2959 | 1168 | 1541 | 1168 | 1168 | 1168 | 1791 |\n" +
            "| Mistress         | 46652 | ???  | 4063 | 4063 | 2264 | 2264 | 1168 | 1168 | 1168 | 1168 | 1791 | 1541 |\n" +
            "| Moonlight        | 40422 | 3586 | 3508 | 3508 | 2558 | 2558 | 1324 | 1002 | 1002 | 1002 | 1002 | 1086 |\n" +
            "| Osiris           | 35880 | 4137 | 5153 | 4702 | 2264 | 2264 | 1168 | 1168 | 1541 | 1541 | 1168 | 1168 |```";
    public static final String l60MVPL = "https://i.imgur.com/A1uiU67.png";
    public static final String l60MVP = "```| Level 60         | HP     | SP    | ATK   | MATK  | DEF   | MDEF  | HIT  | EVA  | CRT  | ASD  | TEN  | CAST |`\n" +
            "| Baphomet         | 122320 | 14887 | 15502 | 15502 | 7654  | 7654  | 3788 | 3788 | 4961 | 5437 | 3788 | 3788 |\n" +
            "| Baphomet Jr.     | 159022 | 14887 | 13403 | 13403 | 9969  | 9969  | 4961 | 3788 | 3788 | 3788 | 3788 | 3788 |\n" +
            "| Dracula          | 159022 | 19398 | 13403 | 13403 | 7654  | 7654  | 3788 | 3788 | 3788 | 3788 | 5751 | 5437 |\n" +
            "| Drake            | 183523 | 14887 | 10301 | 10301 | 12658 | 11531 | 3788 | 4961 | 3788 | 3788 | 3788 | 3788 |\n" +
            "| Eddga            | 201840 | 14887 | 10301 | 10301 | 11531 | 11531 | 3788 | 4961 | 3788 | 3788 | 3788 | 3788 |\n" +
            "| Golden Thief Bug | 183523 | 14887 | 10301 | 10301 | 11531 | 12658 | 3788 | 4961 | 3788 | 3788 | 3788 | 3788 |\n" +
            "| Maya             | 159022 | 19398 | 10301 | 11305 | 9969  | 9969  | 3788 | 4961 | 3788 | 3788 | 3788 | 5751 |\n" +
            "| Mistress         | 159022 | 21309 | 13403 | 13403 | 7654  | 7654  | 3788 | 3788 | 3788 | 3788 | 5751 | 4961 |\n" +
            "| Moonlight        | 137802 | 12896 | 11588 | 11588 | 8628  | 8628  | 4264 | 3262 | 3262 | 3262 | 3262 | 3556 |\n" +
            "| Osiris           | 122320 | 14887 | 17013 | 15502 | 7654  | 7654  | 3788 | 3788 | 4961 | 4961 | 3788 | 3788 |```";
    public static final String l90MVPL = "https://i.imgur.com/lFLL9We.png";
    public static final String l90MVP = "```| Level 90         | HP     | SP    | ATK   | MATK  | DEF   | MDEF  | HIT   | EVA   | CRT   | ASD   | TEN   | CAST  |\n" +
            "| Baphomet         | 267430 | 32927 | 33312 | 33312 | 16634 | 16634 | 7848  | 7848  | 10231 | 11217 | 7848  | 7848  |\n" +
            "| Baphomet Jr.     | 347662 | 32927 | 28833 | 28833 | 21629 | 21629 | 10231 | 7848  | 7848  | 7848  | 7848  | 7848  |\n" +
            "| Dracula          | 347662 | 42838 | 28833 | 28833 | 16634 | 16634 | 7848  | 7848  | 7848  | 7848  | 11871 | 11217 |\n" +
            "| Drake            | 401183 | 32927 | 22171 | 22171 | 27448 | 24991 | 7848  | 10231 | 7848  | 7848  | 7848  | 7848  |\n" +
            "| Eddga            | 441260 | 32927 | 22171 | 22171 | 24991 | 24991 | 7848  | 10231 | 7848  | 7848  | 7848  | 7848  |\n" +
            "| Golden Thief Bug | 401183 | 32927 | 22171 | 22171 | 24991 | 27448 | 7848  | 10231 | 7848  | 7848  | 7848  | 7848  |\n" +
            "| Maya             | 347662 | 42838 | 22171 | 24345 | 21629 | 21629 | 7848  | 10231 | 7848  | 7848  | 7848  | 11871 |\n" +
            "| Mistress         | 347662 | 47079 | 28833 | 28833 | 16634 | 16634 | 7848  | 7848  | 7848  | 7848  | 11871 | 10231 |\n" +
            "| Moonlight        | 301282 | 28516 | 24948 | 24948 | 18728 | 18728 | 8834  | 6762  | 6762  | 6762  | 6762  | 7396  |\n" +
            "| Osiris           | 267430 | 32927 | 36593 | 33312 | 16634 | 16634 | 7848  | 7848  | 10231 | 10231 | 7848  | 7848  |```";
    public static final String addME = "Add me at https://discordapp.com/api/oauth2/authorize?client_id=428176468472889345&permissions=0&scope=bot";

    public static final String skillSet1 = "```md\nSkillset 1 \n" +
            "\n" +
            "Berserk (Enkeli) Trauma/Vuln/Chase (Iawanan) \n" +
            "Bash/Brutal/Mindblow (Haere/Kupona) Dexterity/Wind Walker \n" +
            "(Taika)Pierce/Ghost Arrow/Fire Wall (Asthayi) \n" +
            "\n" +
            "Unyield, Str, Grp Str, Cure\n" +
            "\n" +
            "Equip CP Tank\n" +
            "\n" +
            "Explore Pets: All ATK/MATK```";
    public static final String skillSet2 = "```md\nSkillset 2\n" +
            "\n" +
            "Frost/Stone (Ciernie) Cure (Iawanan) Berserk (Enkeli)\n" +
            "Dexterity/Wind Walker (Taika) Pierce/Ghost Arrow/Fire Wall\n" +
            "(Asthayi)\n" +
            "\n" +
            "Unyield, Str, Grp. Str, Cure\n" +
            "\n" +
            "Equip CP Tank\n" +
            "\n" +
            "Explore Pets: ALL ATK/MATK```";
    public static final String skillSet3 = "```md\nSkillset 3\n" +
            "\n" +
            "Trauma/Vuln/Chase(Desita), Berserk(Haere), Bash/Brutal/Mindblow\n" +
            "(lawanan), Dexterity/Wind Walker(Taika), Pierce/Ghost Arrow/Fire \n" +
            "Wall(Ashtayi)\n" +
            "\n" +
            "Pet 1: Unyield, Str, Grp restore, Grp awaken\n" +
            "Pet 2: Quick spell, Grp stun, Restore, Cure\n" +
            "\n" +
            "Equip CP Tank\n" +
            "\n" +
            "Explore Pets: ALL ATK/MATK```";

    public static final String ssHelp = "```Apache\n" +
            "[Command]\n" +
            "ss Collision\n" +
            "ss Sock Attack\n" +
            "ss Chain Collision\n" +
            "ss Violent Asylum\n" +
            "ss Bleeding Wounds\n" +
            "ss Protection Impact\n" +
            "ss Healing\n" +
            "ss Guardian\n" +
            "ss Substitute Defense\n" +
            "ss DMG Share\n" +
            "ss Holy Sanctuary\n" +
            "ss Passive Virus\n" +
            "ss Vloek\n" +
            "ss Target Guidance\n" +
            "ss Split Attack```\n";

    public static final String ssCollision = "```md\nCollision\n\n" +
            "<Effect: Deals % max HP damage to a single enemy (20% for lv 3).>\n\n" +
            "Unlock 1: Griffin Lv 40\n" +
            "Unlock 2: Elder Willow Lv 100\n" +
            "Unlock 3: Mummy Lv 120\n" +
            "          Devil Maid Lv 120```";

    public static final String ssSockAttack = "```md\nSock Attack\n\n" +
            "<Effect: Single attack that deals % max HP damage to all enemies (12% for lv 3).>\n\n" +
            "Unlock 1: Phen Lv 40\n" +
            "Unlock 2: Sohee Lv 100\n" +
            "Unlock 3: Khalitzburg Lv 120```";

    public static final String ssChainCollision = "```md\nChain Collision\n\n" +
            "<Effect: Multiple attack that deals % max HP damage to all enemies (12% for lv 3).>\n\n" +
            "Unlock 1: Eggyra Lv 40\n" +
            "Unlock 2: Gierth Lv 100\n" +
            "Unlock 3: Incubus Lv 120\n" +
            "          Griffin Lv 120```";

    public static final String ssViolentAsylum = "```md\nViolent Asylum\n\n" +
            "<Effect: Infuses an ally for a duration., increasing damage and decreasing damage taken.>\n\n" +
            "Unlock 1: Marduk Lv 40\n" +
            "Unlock 2: Khalitzburg Lv 100\n" +
            "Unlock 3: Dragon Fly Lv 120\n" +
            "          Anolian Lv 120```";

    public static final String ssBleedingWounds = "```md\nBleeding Wounds\n\n" +
            "<Effect: Decreases all enemies' healing effects by roughly 80% for a duration.>\n\n" +
            "Unlock 1: Devil Maid Lv 40\n" +
            "Unlock 2: Minorus Lv 100\n" +
            "Unlock 3: Wolf Lv 120\n" +
            "          Phen Lv 120```";

    public static final String ssProtectionImpact = "```md\nProtection Impact\n\n" +
            "<Effect: Grants an ally immunity to normal attacks for a duration.>\n\n" +
            "Unlock 1: Anolian Lv 40\n" +
            "Unlock 2: Magnolia Lv 100\n" +
            "Unlock 3: Sohee Lv 120\n" +
            "          Vagabond Wolf Lv 120```";

    public static final String ssHealing = "```md\nHealing\n\n" +
            "<Effect: Heals the weakest ally for large amount of HP (50% at lv 3).>\n\n" +
            "Unlock 1: Wolf Lv 40\n" +
            "Unlock 2: Desert Wolf Lv 100\n" +
            "Unlock 3: Matyr Lv 120\n" +
            "          Magnolia Lv 120```";

    public static final String ssGuardian = "```md\nGuardian\n\n" +
            "<Effect: Allow allies to dodge 1 AoE skill (2 at lv 3) for a duration.>\n\n" +
            "Unlock 1: Vagabond Wolf Lv 40\n" +
            "Unlock 2: Griffin Lv 80\n" +
            "          Marina Lv 80\n" +
            "Unlock 3: Peco Peco Lv 120 \n" +
            "          Nine Tail Lv 120```";

    public static final String ssSubstituteDefense = "```md\nSubstitute Defense\n\n" +
            "<Effect: Pets will take fatal damage for the character for a duration.>\n\n" +
            "Unlock 1: Matyr Lv 40\n" +
            "Unlock 2: Eggyra Lv 80\n" +
            "          Gierth Lv 80\n" +
            "Unlock 3: Elder Willow Lv 120\n" +
            "          Golem Lv 120```";

    public static final String ssDMGShare = "```md\nDMG Share\n\n" +
            "<Effect: Allies will take damage for the weakest ally for a duration.>\n\n" +
            "Unlock 1: Chonchon Lv 40\n" +
            "Unlock 2: Vagabond Wolf Lv 80\n" +
            "          Gierth Lv 80\n" +
            "Unlock 3: Sohee Lv 120\n" +
            "          Eggyra Lv 120```";

    public static final String ssHolySanctuary = "```md\nHoly Sanctuary\n\n" +
            "<Effect: Grants an ally immunity to 1 single-target skill (2 skills at lv 3) for a duration.>\n\n" +
            "Unlock 1: Mummy Lv 40\n" +
            "Unlock 2: Golem Lv 100\n" +
            "Unlock 3: Devil Maid Lv 120\n" +
            "          Marduk Lv 120```";

    public static final String ssPassiveVirus = "```md\nPassive Virus\n\n" +
            "<Effect: Disables opponent Deathwhisper effect (ie. Mistress revive) for a duration.>\n\n" +
            "Unlock 1: Incubus Lv 40\n" +
            "Unlock 2: Dragon Fly Lv 100\n" +
            "Unlock 3: Marina Lv 120\n" +
            "          Anolian Lv 120```";

    public static final String ssVloek = "```md\nVloek\n\n" +
            "<Effect: Disables opponent rune effects for a duration.>\n\n" +
            "Unlock 1: Marina Lv 40\n" +
            "Unlock 2: Anubis Lv 100\n" +
            "Unlock 3: Elder Willow Lv 120\n" +
            "          Nine Tail Lv 120```";

    public static final String ssTargetGuidance = "```md\nTarget Guidance\n\n" +
            "<Effect: Guides all allies to attack the weakest enemy for a duration.>\n\n" +
            "Unlock 1: Nine Tail Lv 40\n" +
            "Unlock 2: Phen Lv 80\n" +
            "          Incubus Lv 80\n" +
            "Unlock 3: Chonchon Lv 120\n" +
            "          Mummy Lv 120```";

    public static final String ssSplitAttack = "```md\nSplit Attack\n\n" +
            "<Effect: Attacks and skills will deal 15% splash damage for a duration.>\n\n" +
            "Unlock 1: Peco Peco Lv 40\n" +
            "Unlock 2: Golem Lv 80\n" +
            "          Khalitzburg Lv 80\n" +
            "Unlock 3: Marduk Lv 120\n" +
            "          Chonchon Lv 120```";

    public static String petHelp = "```Apache\n" +
            "[Command]   : [Description]\n" +
            "pet orange  : orange pets\n" +
            "pet purple  : purple pets\n" +
            "pet blue    : blue pets\n" +
            "pet green   : green pets\n" +
            "pet white   : white pets```";

    public static String petGreen = "```md\n" +
            "**List of green pets**\n\n" +
            "Andre           Munak\n" +
            "Evil Skeleton   Poring\n" +
            "Goblin -_-      Rocker\n" +
            "Goblin :(       Spore\n" +
            "Goblin :D       Tarou\n" +
            "Goblin :o       Thief Bug\n" +
            "Hydra           Vadon\n" +
            "Mandragora      Whisper\n" +
            "Marc            Willow```";

    public static String petOrange = "```md\n" +
            "**List of MVP pets**\n\n" +
            "Atroce              Maya\n" +
            "Baphomet            Mistress\n" +
            "Baphomet Jr.        Moonlight Flower\n" +
            "Detardeurus         Orc Hero\n" +
            "Dracula             Orc Lord\n" +
            "Drake               Osiris\n" +
            "Eddga               Phreeoni\n" +
            "Golden Thief Bug    Randgris```";

    public static String petPurple = "```md\n" +
            "**List of purple pets**\n\n" +
            "Angeling       Isis              Petite Dragon\n" +
            "Anubis         Jakk              Rotar Zairo\n" +
            "Deniro         Joker             Santa Poring\n" +
            "Devil Maid     Khalitzburg       Smokie\n" +
            "Devling        King Goblin       Sohee\n" +
            "Dragon Fly     King Poring       Steam Goblin\n" +
            "Elder Willow   Marionette        Sting\n" +
            "Ghostring      Minorous          Strouf\n" +
            "Giant Whisper  Mutant Dragonoid  Vagabond Wolf\n" +
            "Greet Petit    Nightmare\n" +
            "Griffin        Nine Tail\n" +
            "High Orc       Obeaune\n" +
            "Hyegun         Orc Zombie\n" +
            "Incubus        Peco Peco```";

    public static String petWhite = "```md\n" +
            "**List of white pets**\n\n" +
            "Fabre\n" +
            "Lunatic\n" +
            "Piere```";

    public static String petBlue = "```md\n" +
            "**List of blue pets**\n\n" +
            "Archer Skeleton  Hornet\n" +
            "Argiope          Muka\n" +
            "Bigfoot          Orc Archer\n" +
            "Bon Gun          Orc Baby\n" +
            "Creamy           Pirate Skeleton\n" +
            "Deviruchi        Poporing\n" +
            "Drainliar        Savage\n" +
            "Drops            Thara Frog\n" +
            "Eclipse          Vitata\n" +
            "Farmiliar        Warrior Orc\n" +
            "Gargoyle         Yoyo\n" +
            "Goblin Archer```";
}
